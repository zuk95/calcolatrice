import javax.swing.*;

public class ButtonCalc extends JButton {
    private int key;

    public ButtonCalc(String text,int key) {
        super(text);
        this.key=key;
    }

    public int getKey() {
        return key;
    }
}
