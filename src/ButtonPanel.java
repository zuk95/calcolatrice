import calcolatriceModel.Calcolatrice;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonPanel extends JPanel {
    JButton uno = new ButtonCalc("7", Calcolatrice.BUTTON_7);
    JButton due = new ButtonCalc("8",Calcolatrice.BUTTON_8);
    JButton tre = new ButtonCalc("9",Calcolatrice.BUTTON_9);
    JButton qua = new ButtonCalc("+",Calcolatrice.BUTTON_PLUS);
    JButton cin = new ButtonCalc("CE",Calcolatrice.BUTTON_CE);
    JButton sei = new ButtonCalc("4",Calcolatrice.BUTTON_4);
    JButton set = new ButtonCalc("5",Calcolatrice.BUTTON_5);
    JButton ott = new ButtonCalc("6",Calcolatrice.BUTTON_6);
    JButton nov = new ButtonCalc("-",Calcolatrice.BUTTON_MINUS);
    JButton die = new ButtonCalc("M+",Calcolatrice.BUTTON_M);
    JButton und = new ButtonCalc("1",Calcolatrice.BUTTON_1);
    JButton dod = new ButtonCalc("2",Calcolatrice.BUTTON_2);
    JButton trd = new ButtonCalc("3",Calcolatrice.BUTTON_3);
    JButton qtr = new ButtonCalc("X",Calcolatrice.BUTTON_MOLT);
    JButton qui = new ButtonCalc("MR",Calcolatrice.BUTTON_MR);
    JButton sed = new ButtonCalc("0",Calcolatrice.BUTTON_0);
    JButton dic = new ButtonCalc(".",Calcolatrice.BUTTON_COMMA);
    JButton dct = new ButtonCalc("=",Calcolatrice.BUTTON_RETURN);
    JButton dcn = new ButtonCalc("/",Calcolatrice.BUTTON_DIV);
    JButton ven = new ButtonCalc("MC",Calcolatrice.BUTTON_MC);

    public ButtonPanel() {
        GridLayout l = new GridLayout(4,5);
        l.setHgap(20);
        l.setVgap(20);
        setLayout(l);
        add(uno);add(due);add(tre);add(qua);
        add(cin);add(sei);add(set);add(ott);
        add(nov);add(die);add(und);add(dod);
        add(trd);add(qtr);add(qui);add(sed);
        add(dic);add(dct);add(dcn);add(ven);
    }

    public void addListener(ActionListener al){
        uno.addActionListener(al);
        due.addActionListener(al);
        tre.addActionListener(al);
        qua.addActionListener(al);
        cin.addActionListener(al);
        sei.addActionListener(al);
        set.addActionListener(al);
        ott.addActionListener(al);
        nov.addActionListener(al);
        die.addActionListener(al);
        und.addActionListener(al);
        dod.addActionListener(al);
        trd.addActionListener(al);
        qtr.addActionListener(al);
        qui.addActionListener(al);
        sed.addActionListener(al);
        dic.addActionListener(al);
        dct.addActionListener(al);
        dcn.addActionListener(al);
        ven.addActionListener(al);
    }


}
