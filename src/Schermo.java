import javax.swing.*;
import java.awt.*;

public class Schermo extends JLabel {
    public Schermo() {
        setHorizontalAlignment(JLabel.RIGHT);
        setOpaque(true);
        setBackground(Color.BLACK);
        setForeground(Color.GREEN);
        setText("ok");
    }
}
