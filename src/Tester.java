import calcolatriceModel.Calcolatrice;

import javax.swing.*;

public class Tester {
    public static void main(String[] args) {
        Calcolatrice calcolatrice = new Calcolatrice();
        FrameCalcolatrice f = new FrameCalcolatrice(calcolatrice);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
}
