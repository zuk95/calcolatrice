import calcolatriceModel.Calcolatrice;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FrameCalcolatrice extends JFrame {
    private Calcolatrice calcolatrice;
    private Schermo schermo;
    private ButtonPanel panBottoni;


    public FrameCalcolatrice(Calcolatrice calcolatrice) {
        this.calcolatrice=calcolatrice;
        setTitle("CALCULATOR");
        setSize(600,400);
        setResizable(false);
        BorderLayout l = new BorderLayout();
        getContentPane().setLayout(l);
        schermo = new Schermo();
        panBottoni = new ButtonPanel();
        add(schermo,l.CENTER);
        add(panBottoni,l.SOUTH);
        panBottoni.addListener(al);

    }

    ActionListener al = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e){
            calcolatrice.key(((ButtonCalc)e.getSource()).getKey());
            schermo.setText(calcolatrice.getDisplay());

        }
    };


}